# SATL
## Sistema Analizador de Textos Literarios


El presente repositorio contiene el código utilizado en el Trabajo Terminal 2021 - A066, el cual esta enfocado a la creación de un sistema capaz de analizar textos literarios en formato digital para su posterior clasificación.

### Programas necesarios para su instalación

	* React 17.0.2
	* React-router-dom 6.2.2
	* Sass
	
	* Django 3.1.7
	
	* Flask 1.1.2
	
	* PostgreSQL 13
	
	* Numpy 1.20.2
	* Psycopg2 2.8.6
	* PyMuPDF 1.19.1
	* Fitz
	* Nltk 3.6.7
	* Stanza 1.3.0
	* Sklearn
	* matplotlib 3.5.1
	* pandas 1.2.4

### Actualización 24 - 03 - 2022
  * Se creo el repositorio con los avances obtenidos.
  * Se dividió el sistema, migrando de Django a un conjunto de diferentes tecnologías.
  * Se continua con la migración de Django templates a React
  * Se realiza la conexión Postgres – Flask – Django, la cual sustituye a la conexión Django – Postgres
  * Se agregaron scripts con códigos en Python para el análisis, utilizando NPL y Machine Learning

### Actualziacion 07 - 04 - 2022
  * Se crean scripts para creacion de base de datos, este incluye:
	* Creacion de tablas
	* Creacion de secuencias
	* Creacion de funciones
	* Creacion de vistas
	* Creacion de triggers
	* Llenado de catalogos y datos iniciales
  * Se crean scripts para conexion con la base de datos utilizando Flask
  * Se crea json para pruebas de conexion Flask - PostgreSQL

### Actualizacion 15 - 04 - 2022
  * Se creo clase con clasificador en carpeta dcm potenciado con algoritmos de agrupación, K NN y K means.
  * Se creo clase para graficar datos de entrenamiento y de prueba
  * Se comienza con la creacion de servidor ecargado de almacenar y comunicar datos de clasificador y servidor web.

### Actualizacion 19 - 04 - 2022
  * Se crea servidor para analisis, el cual administra las peticiones de clasificacion y entrenamiento
  * Se crea administrador de datos, el cual gestiona los datos y el procesamiento de estos
  * Se crea proceso multi hilo para mejorar el rendimiento del sistema analizador


### Actualizacion 03 - 05 - 2022
  * Se crean nuevas vistas, para obtener usuarios creados y libros analizados en la base de datos.
  * Se concluye con la conexion del Front, Back y base de datos. 
  * Se crea manejo de sesiones y cookies para inicio de sesion.
  * Se crean nuevas vistas, concluyendo con la creacion de estas.
  * Se crearon nuevos estilos para componentes en Front.
  

